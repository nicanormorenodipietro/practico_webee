import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SensorsListComponent } from './components/sensors-list/sensors-list.component';
import { SensorEventsListComponent } from './components/sensor-events-list/sensor-events-list.component';
import { SensorEventCreateComponent } from './components/sensor-event-create/sensor-event-create.component';
import { SensorCreateComponent } from './components/sensor-create/sensor-create.component';

@NgModule({
  declarations: [
    AppComponent,
    SensorsListComponent,
    SensorEventsListComponent,
    SensorEventCreateComponent,
    SensorCreateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
