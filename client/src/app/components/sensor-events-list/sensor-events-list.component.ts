import { Component, OnInit } from '@angular/core';
import {SensorEventService} from '../../services/sensorEvent.service'

@Component({
  selector: 'app-sensor-events-list',
  templateUrl: './sensor-events-list.component.html',
  styleUrls: ['./sensor-events-list.component.css'],
  providers: [SensorEventService]
})
export class SensorEventsListComponent implements OnInit {

  constructor(
    private _sensorEventService: SensorEventService
    ) { 

  }

  ngOnInit() {
    let sensorEventList = this._sensorEventService.getSesnorEventsList(null).subscribe(
      response =>{
        console.log("response", response)
        response
      },
      error=>{
        console.log('error', error)
      }
    );
    console.log('rta',sensorEventList)
  }

  getSensorNextPage(index){
    let sensorEventList = this._sensorEventService.getSesnorEventsList(index).subscribe(
      response =>{
        console.log("response", response)
        response
      },
      error=>{
        console.log('error', error)
      }
    );
    console.log('rta',sensorEventList)
  }

  deleteSensor(index){
    let deletedSensorEvent = this._sensorEventService.deleteSensorEvent(index).subscribe(
      response =>{
        console.log("response", response)
        response
      },
      error=>{
        console.log('error', error)
      }
    );
    console.log('reta',deletedSensorEvent)
  }


}
