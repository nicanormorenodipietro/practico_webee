import { Component, OnInit } from '@angular/core';
import {SensorEvent} from '../../models/sensorEvent'
import {SensorEventService} from '../../services/sensorEvent.service'


@Component({
  selector: 'app-sensor-event-create',
  templateUrl: './sensor-event-create.component.html',
  styleUrls: ['./sensor-event-create.component.css'],
  providers: [SensorEventService]
})
export class SensorEventCreateComponent implements OnInit {
  public SensorEvent : SensorEvent

  constructor(
    private _sensorEventService:SensorEventService
    ) { 
      this.SensorEvent = new SensorEvent(null,null,null,null)
  }

  ngOnInit() {
  }

  createSensorEvent(sensorEvent){
    var rta =  this._sensorEventService.createSensoreEvent(sensorEvent).subscribe(
      response =>{
        console.log("respose", response)
      },
      error=>{
        console.log('error', error)
      }
    );
    console.log('rta',rta)
  }

  updateSensorEvent(sensorEvent){
    var rta =  this._sensorEventService.updateSensorEvent(sensorEvent).subscribe(
      response =>{
        console.log("response", response)
      },
      error=>{
        console.log('error', error)
      }
    );
    console.log('rta',rta)
  }

  onSubmit(){
    console.log(this.SensorEvent)
  }

}
