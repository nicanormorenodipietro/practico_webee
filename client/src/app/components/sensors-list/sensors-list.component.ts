import { Component, OnInit } from '@angular/core';
import {Sensor} from '../../models/sensor'
import {SensorService} from '../../services/sensor.service'

@Component({
  selector: 'app-sensors-list',
  templateUrl: './sensors-list.component.html',
  styleUrls: ['./sensors-list.component.css'],
  providers:[SensorService]
})

export class SensorsListComponent implements OnInit {
  
  constructor( 
    private _sensorService:SensorService
    ) { 
      
    }

  ngOnInit() {
    let sensorList = this._sensorService.getSensorsList(null).subscribe(
      response =>{
        console.log("response", response)
        response
      },
      error=>{
        console.log('error', error)
      }
    );
    console.log('rta',sensorList)
  }

  getSensorNextPage(index){
    let sensorList = this._sensorService.getSensorsList(index).subscribe(
      response =>{
        console.log("response", response)
        response
      },
      error=>{
        console.log('error', error)
      }
    );
    console.log('rta',sensorList)
  }

  deleteSensor(index){
    let deletedSensor = this._sensorService.deleteSensor(index).subscribe(
      response =>{
        console.log("response", response)
        response
      },
      error=>{
        console.log('error', error)
      }
    );
    console.log('reta',deletedSensor)
  }

}
