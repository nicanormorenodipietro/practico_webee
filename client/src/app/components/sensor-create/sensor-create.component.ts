import { Component, OnInit } from '@angular/core';
import {Sensor} from '../../models/sensor'
import {SensorService} from '../../services/sensor.service'

@Component({
  selector: 'app-sensor-create',
  templateUrl: './sensor-create.component.html',
  styleUrls: ['./sensor-create.component.css'],
  providers:[SensorService]
})
export class SensorCreateComponent implements OnInit {
  public Sensor : Sensor
  constructor(private _sensorService : SensorService) {
    this.Sensor = new Sensor("","",null,null,null,null)
    this.Sensor = Object.assign(this.Sensor, {location:{lat:null, lng:null}})
   }

  ngOnInit() {

  }

  createSensor(sensor){
    var rta =  this._sensorService.createSensor(sensor).subscribe(
      response =>{
        console.log("respose", response)
      },
      error=>{
        console.log('error', error)
      }
    );
    console.log('rta',rta)
  }

  updateSensor(sensor){
    var rta =  this._sensorService.updateSensor(sensor).subscribe(
      response =>{
        console.log("response", response)
      },
      error=>{
        console.log('error', error)
      }
    );
    console.log('rta',rta)
  }

  onSubmit(){
    console.log(this.Sensor)
  }

}
