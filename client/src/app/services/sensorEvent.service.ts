import {Injectable} from '@angular/core'
import {Http, Response, Headers} from '@angular/http'
import 'rxjs/add/operator/map'
import {Observable} from 'rxjs/Observable'
import {GLOBAL} from './Settings'

@Injectable()
export class SensorEventService{
    public url: string

    constructor(private _http: Http){
        this.url = GLOBAL.url
    }

    createSensoreEvent(sensorEvent){
        var json = JSON.stringify(sensorEvent)
        var params = json

        var headers = new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})

        return this._http.post(this.url + '/add-sensorEvent', params, {headers:headers})
    }

    updateSensorEvent(sensorEvent){
        var json = JSON.stringify(sensorEvent)
        var params = json

        var headers = new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})

        return this._http.post(this.url + '/update-sensorEvent', params, {headers:headers})
    }

    getSesnorEventsList(page){
        var headers = new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
        let url = this.url + '/get-sensorEvent'
        if(page){
            url = url + '/' + page
        }
        return this._http.get(url, {headers:headers})
    }

    deleteSensorEvent(sensorEventId){
        var headers = new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
        return this._http.delete(this.url + '/delete-sensorEvent/' + sensorEventId, {headers:headers})
    }
}