import {Injectable} from '@angular/core'
import {Http, Response, Headers} from '@angular/http'
import 'rxjs/add/operator/map'
import {Observable} from 'rxjs/Observable'
import {GLOBAL} from './Settings'



@Injectable()
export class SensorService{
    public url: string;

    constructor(private _http:Http){
        this.url = GLOBAL.url;
    }

    createSensor(sensor){
        let json = JSON.stringify(sensor)
        let params = json
        let headers= new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
    
        return this._http.post(this.url + '/new-sensor', params,{headers:headers})
    }

    updateSensor(sensor){
        let json = JSON.stringify(sensor)
        let params = json
        let headers= new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
    
        return this._http.post(this.url + '/update-sensor', params,{headers:headers})
    }

    getSensorsList(page){
        let headers= new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
        let url = this.url + '/get-all-sensorEvents'
        if(page)    url = url + '/' + page   
        return this._http.get(url, {headers:headers})   
    }

    deleteSensor(sensorId){
        return this._http.delete(this.url + '/delete-sensor/' + sensorId, sensorId);
    }

}