export class SensorEvent{
    constructor(
        public id: string,
        public sensorId: string,
        public createAt: Date,
        public value: number
 
    ){}
}