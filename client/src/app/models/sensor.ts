export class Sensor{
    constructor(
        id: string,
        name: string,
        location: {
            lat:number,
            lng:number
        },
        active: boolean,
        minValue: number,
        maxValue: number
    ){}
}