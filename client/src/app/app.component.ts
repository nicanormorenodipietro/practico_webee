import { Component, OnInit } from '@angular/core';
import {SensorService} from './services/sensor.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[SensorService]

})
export class AppComponent {
  title = 'app works!';
}
