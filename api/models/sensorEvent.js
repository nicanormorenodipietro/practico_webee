'use strinct'

var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var SensorEventSchema = Schema({
    id: String,
    sensorId:{type: Schema.ObjectId, ref:'Sensor'},
    createAt:Date,
    value:Number
})

module.exports = mongoose.model('SensorEvent', SensorEventSchema)