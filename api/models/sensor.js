'use strinct'

var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var SensorSchema = Schema({
    id: String,
    name: String,
    location:{
        lat:Number,
        lng:Number
    },
    active:Boolean,
    minValue:Number,
    maxValue:Number
})

module.exports = mongoose.model('Sensor', SensorSchema)


