'use strict'

var express = require('express');
var SensorController = require('../controllers/sensor')

var api = express.Router()
api.post('/add-sensor', SensorController.newSensor)
api.get('/get-sensor/:id', SensorController.getSensor)
api.get('/get-all-sensors/:page?', SensorController.getAllSensorsPaginated)
api.put('/update-sensor/:id', SensorController.updateSensor)
api.delete('/delete-sensor/:id', SensorController.deleteSensor)

module.exports = api