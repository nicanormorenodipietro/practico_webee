'use strict'

var express = require('express');
var SensorEventController = require('../controllers/sensorEvent')

var api = express.Router()
api.post('/add-sensorEvent', SensorEventController.newSensorEvent)
api.get('/get-sensorEvent/:id', SensorEventController.getSensorEvent)
api.get('/get-all-sensorsEvent/:page?', SensorEventController.getAllSensorEventsPaginated)
api.put('/update-sensorEvent/:id', SensorEventController.updateSensorEvent)
api.delete('/delete-sensorEvent/:id', SensorEventController.deleteSensorEvent)

module.exports = api