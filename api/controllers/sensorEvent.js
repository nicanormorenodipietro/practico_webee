'use strict'

var SensorEvent = require('../models/sensorEvent')

function newSensorEvent(req,res){
    var params = req.body
    var sensorEvent = new SensorEvent()
    
    sensorEvent.id = param.id
    sensorEvent.sensorId = params.body.sensorId
    sensorEvent.createAt = params.body.createAt
    sensorEvent.value = params.body.value
    
    sensorEvent.save((error, sensorEventStored)=>{
        if(error){
            res.status(500).send({message:'error al guardar sensor'})
        }else{
            if(!sensorEventStored){
                res.status(404).send({message:'sensor no encontrado'})
            }else{
               return res.status(200).send({sensor:sensorEventStored})
            }
        }
    })
}

function getSensorEvent(req,res){
    var sensorEventId = req.params.sensorEventId
    SensorEvent.findById(sensorEventId, (error,sensorEventStored)=>{
        if(error){
            res.status(500).send({message:'error en la peticióin'})
        }else{
            if(!sensorEventStored){
                res.status(404).send({message:'sensor no encontrado'})
            }else{
                return res.status(200).send({sensor:sensorEventStored})
            }
        }
    })
}

function getAllSensorEventsPaginated(req,res){
    var page 
    var itemsPerPage 
    req.params.itemsPerPage ? itemsPerPage= req.params.itemsPerPage : itemsPerPage = 5
    req.params.page ? page = req.params.page : page = 1

    SensorEvent.find().sort('name').paginate(page, itemsPerPage, (error, sensorEvents, totalItems)=>{
        if(error){
            res.status(500).send({message:'error en la peticióin'})
        }else{
            if(!sensors){
                res.status(404).send({message:'no hay sensores registrados'})
            }else{
               return res.status(200).send({
                pages:totalItems,   
                sensorEventsList:sensorEvents
                })
            }
        }
    })
}

function updateSensorEvent(req, res){
    var sensorEventId = req.params.id
    var update = req.body

    SensorEvent.findByIdAndUpdate(sensorEventId, update, (error, sensorEventUpdate)=>{
        if(error){
            res.status(500).send({message:'error en la peticióin'})
        }else{
            if(!sensorEventUpdate){
                res.status(404).send({message:'no ha sido posible actualizar el evento de sensor'})
            }else{
               return res.status(200).send({ 
                sensorEvent:sensorEventUpdate
                })
            }
        }
    })
}

function deleteSensorEvent(req,res){
    var sensorEventId = req.params.id
    Sensor.findByIdAndRemove(sensorEventId,(error, sensorEventRemoved)=>{
        if(error){
            res.status(500).send({message:'error en la peticióin'})
        }else{
            if(!sensorRemoved){
                res.status(404).send({message:'no ha sido posible eliminar el evento de sensor'})
            }else{
               return res.status(200).send({sensorEventRemoved:sensorEventRemoved})
            }
        }
    })
}
module.exports={
    newSensorEvent,
    getSensorEvent,
    getAllSensorEventsPaginated,
    updateSensorEvent,
    deleteSensorEvent
}