'use strict'

var Sensor = require('../models/sensor')
var SensorEvent = require('../models/sensorEvent')
var mongoonsePaginate = require('mongoose-pagination')


function newSensor(req,res){
    var params = req.body
    var sensor = new Sensor()
  
    sensor.name = params.name;
    sensor.location.lat = params.location.lat ;
    sensor.location.lng = params.location.lng ;
    sensor.active= params.active;
    sensor.minValue = params.minValue ;
    sensor.maxValue= params.maxValue;
    
    sensor.save((error, sensorStored)=>{
        if(error){
            res.status(500).send({message:'error al guardar sensor'})
        }else{
            if(!sensorStored){
                res.status(404).send({message:'sensor no encontrado'})
            }else{
               return res.status(200).send({sensor:sensorStored})
            }
        }
    })
}

function getSensor(req,res){
    var sensorId = req.params.sensorId
    Sensor.findById(sensorId, (error,sensorStored)=>{
        if(error){
            res.status(500).send({message:'error en la peticióin'})
        }else{
            if(!sensorStored){
                res.status(404).send({message:'sensor no encontrado'})
            }else{
                return res.status(200).send({sensor:sensorStored})
            }
        }
    })
}

function getAllSensorsPaginated(req,res){
    var page 
    var itemsPerPage 
    req.params.itemsPerPage ? itemsPerPage= req.params.itemsPerPage : itemsPerPage = 5
    req.params.page ? page = req.params.page : page = 1

    Sensor.find().sort('name').paginate(page, itemsPerPage, (error, sensors, totalItems)=>{
        if(error){
            res.status(500).send({message:'error en la peticióin'})
        }else{
            if(!sensors){
                res.status(404).send({message:'no hay sensores registrados'})
            }else{
                return res.status(200).send({
                pages:totalItems,   
                sensorsList:sensors
                })
            }
        }
    })
}

function updateSensor(req, res){
    var sensorId = req.params.id
    var update = req.body

    Sensor.findByIdAndUpdate(sensorId, update, (error, sensorUpdate)=>{
        if(error){
            res.status(500).send({message:'error en la peticióin'})
        }else{
            if(!sensorUpdate){
                res.status(404).send({message:'no ha sido posible actualizar el sensor'})
            }else{
               return res.status(200).send({ 
                sensor:sensorUpdate
                })
            }
        }
    })
}

function deleteSensor(req,res){
    var sensorId = req.params.id
    
    Sensor.findByIdAndRemove(sensorId,(error, sensorRemoved)=>{
        if(error){
            res.status(500).send({message:'error en la peticióin'})
        }else{
            if(!sensorRemoved){
                res.status(404).send({message:'no ha sido posible eliminar el sensor'})
            }else{
                SensorEvent.find({sensorId : sensorRemoved._id}).remove((error, sensorEventRemoved)=>{
                    if(error){
                        res.status(500).send({message:'error en la peticióin'})
                    }else{
                        if(!sensorRemoved){
                            res.status(404).send({message:'no ha sido posible eliminar el sensor event'})
                        }else{
                            return res.status(200).send({sensorRemoved:sensorRemoved})
                        }
                    }
                })
            }
        }
    })
}

module.exports={
    newSensor,
    getSensor,
    getAllSensorsPaginated,
    updateSensor,
    deleteSensor
}