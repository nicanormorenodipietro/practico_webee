'use strict'

var express = require('express')
var bodyParser = require('body-parser')

var app = express()
//routes
var sensor_routes = require('./routes/sensor')
var sensorEvent_routes = require('./routes/sensorEvent')

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//headers
app.use((req,res, next)=>{
    res.header('Acces-Control-Origin','*');
    res.header(
        'Acces-Control-Allow-Headers',
        'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Acces-Control-Allow-Request-Method'
        )
        res.header('Acces-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
        res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

        next();
})

//base routes
app.use('/api', sensor_routes);
app.use('/api', sensorEvent_routes);

module.exports = app;