'use strict'

var mongoose = require('mongoose')
var app = require('./app')
var port = process.env.PORT || 3977;

mongoose.connect('mongodb://localhost:27017/practico', (err,res)=>{
    if(err){
        throw err;
    }
    else{
        console.log('all is fine')
        app.listen(port, function(){
            console.log('server rest api listening http://localhost:'+ port)
        })
    }
})